# Likes problem refactoring

```ruby
likes [] # must be "no one likes this"

likes ["John"] # must be "John likes this"

likes ["John", "Lili"] # must be "John and Lili like this"

likes ["John", "John", "Mark"] # must be "John, Lili and Mark like this"

likes ["John", "Lili", "Mark", "Ema"] # "John, Lili and 2 others like this"
```
