class DisplayMessageV4
  # Writing the code so it looks similar
  # Find the similar parts and look for the abstraction
  def self.for(names)
    case names.size
    when 0
      "no one #{action(false)}"
    when 1
      "#{ names[0] } #{action(false)}" 
    when 2
      "#{ names[0] } and #{names[1]} #{action}" 
    when 3
      "#{ names[0] }, #{ names[1] } and #{ names[2] } #{action}"
    else
      "#{ names[0] }, #{ names[1] } and #{ names.size - 2 } others #{action}"
    end
  end

  # Probably we could come up witth a better name here
  # This helps us to move forwards and is good enough for now
  def self.action(multiple = true)
    if multiple 
      'like this'
    else
      'likes this'
    end
  end
end

