class DisplayMessageV8
  # Writing the code so it looks similar
  # Find the similar parts and look for the abstraction
  def self.for(names)
    named_users = names_list(names)
    other_names = others(names.size)
    likes_action = action(names.size)

    [named_users, other_names, likes_action].reject(&:empty?).join(' ')
  end

  # Probably we could come up witth a better name here
  # This helps us to move forwards and is good enough for now
  def self.action(number)
    if number > 1
      'like this'
    else
      'likes this'
    end
  end

  def self.others(number)
    if number > 3
      "and #{number - 2} others"
    else
      ""
    end
  end

  def self.names_list(names)
    case names.size
    when 0
      "no one"
    when 1
      names[0]
    when 2
      "#{names[0]} and #{names[1]}"
    when 3
      "#{names[0]}, #{names[1]} and #{ names[2] }"
    else
       "#{names[0]}, #{names[1]}"
    end
  end
end

