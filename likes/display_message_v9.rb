class DisplayMessageV9
  def self.for(names)
    NamesList.for(names).to_s
  end
end

class NamesList
  attr_reader :names
  
  def initialize(names)
    @names = names
  end
  
  def named_users
     "#{names[0]}, #{names[1]}"
  end

  def size 
    names.size
  end

  def action
    'like this'
  end

  def others
    "and #{size - 2} others"
  end

  def to_s

    # binding.pry
    [named_users, others, action].reject(&:empty?).join(' ')
  end

  def self.for(names)
    case names.size
    when 0
      EmptyNameList
    when 1
      NameList1
    when 2
      NameList2
    when 3
      NameList3
    else
      NamesList
    end.new(names)
  end
end

class EmptyNameList < NamesList
  def named_users
    'no one'
  end

  def action
    'likes this'
  end

  def others
    ""
  end
end

class NameList1 < NamesList
  def named_users
    "#{names[0]}"
  end

  def action
    'likes this'
  end
  
  def others
    ""
  end  
end

class NameList2 < NamesList
  def named_users
    "#{names[0]} and #{names[1]}"
  end

  def others
    ""
  end  
end

class NameList3 < NamesList
  def named_users
    "#{names[0]}, #{names[1]} and #{ names[2] }"
  end

  def others
    ""
  end  
end