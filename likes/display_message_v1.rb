class DisplayMessageV1
  def self.for(names)
    return 'no one likes this' if names.size == 0
    return "#{ names.first } likes this" if names.size == 1
    return "#{ names.join(' and ') } like this" if names.size == 2
    return "#{ names[0] }, #{ names[1] } and #{ names[2] } like this" if names.size == 3
    return "#{ names[0] }, #{ names[1] } and #{ names.size - 2 } others like this" if names.size > 3
  end
end