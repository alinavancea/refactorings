class DisplayMessageV6
  # Writing the code so it looks similar
  # Find the similar parts and look for the abstraction
  def self.for(names)
    case names.size
    when 0
      [names_list(names), others(names.size), action(false)].reject(&:empty?).join(' ')
    when 1
      [names_list(names), others(names.size), action(false)].reject(&:empty?).join(' ')
    when 2
      [names_list(names), others(names.size), action].reject(&:empty?).join(' ')
    when 3
      [names_list(names), others(names.size), action].reject(&:empty?).join(' ')
    else
      [names_list(names), others(names.size), action].reject(&:empty?).join(' ')
    end
  end

  # Probably we could come up witth a better name here
  # This helps us to move forwards and is good enough for now
  def self.action(multiple = true)
    if multiple 
      'like this'
    else
      'likes this'
    end
  end

  def self.others(number)
    if number > 3
      "and #{number - 2} others"
    else
      ""
    end
  end

  def self.names_list(names)
    case names.size
    when 0
      "no one"
    when 1
      names[0]
    when 2
      "#{names[0]} and #{names[1]}"
    when 3
      "#{names[0]}, #{names[1]} and #{ names[2] }"
    else
       "#{names[0]}, #{names[1]}"
    end
  end
end

