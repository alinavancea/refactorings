require 'minitest/autorun'
require 'pry'

require_relative 'display_message_v1'
require_relative 'display_message_v2'
require_relative 'display_message_v3'
require_relative 'display_message_v4'
require_relative 'display_message_v5'
require_relative 'display_message_v6'
require_relative 'display_message_v7'
require_relative 'display_message_v8'
require_relative 'display_message_v9'

class DisplayMessageTest < Minitest::Test

  def message_class
    DisplayMessageV9
  end

  def test_with_no_like
    assert message_class.for([]) == "no one likes this"
  end

  def test_with_1_like
    assert message_class.for(['John']) == "John likes this"
  end

  def test_with_2_like
    assert message_class.for(['John', 'Lili']) == "John and Lili like this"
  end  

  def test_with_3_like
    assert message_class.for(['John', 'Lili', 'Mark']) == "John, Lili and Mark like this"
  end 

  def test_with_4_like
    assert message_class.for(['John', 'Lili', 'Mark', 'Ema']) == "John, Lili and 2 others like this"
  end

  def test_with_6_like
    assert message_class.for(['John', 'Lili', 'Mark', 'Ema', 'Bob', 'Lyla']) == "John, Lili and 4 others like this"
  end  
end